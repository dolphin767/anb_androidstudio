package com.example.anb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.example.anb.sell.SellActivity;
import com.example.anb.sell.SellActivity_flip;

import java.util.ArrayList;

/**
 * Created by edward on 2015-04-02.
 */
public class Sell extends Fragment {
    View v;
    private ProgressDialog pDialog;
    String JSONString;
    private StaggeredGridView mGridView;
    private boolean mHasRequestedMore;
    private SellAdapter mAdapter;
    private ArrayList<PostInfo_simple> mData;


    public Sell(String JSONString) {
        this.JSONString = JSONString;
    }
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_sell, container, false);
        return v;

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGridView = (StaggeredGridView) getView().findViewById(R.id.grid_view);
        PostInfo_simple sell_data = new PostInfo_simple();
        sell_data.setuserText("판매하기");
        //code_data.setuserProfile("");
        sell_data.setuserName("");
        sell_data.setproductPrice(0);
        sell_data.setproductName("");
        /*
        if (savedInstanceState == null) {
            final LayoutInflater layoutInflater = getActivity().getLayoutInflater();

            View header = layoutInflater.inflate(R.layout.list_item_header_footer, null);
            View footer = layoutInflater.inflate(R.layout.list_item_header_footer, null);
            TextView txtHeaderTitle = (TextView) header.findViewById(R.id.txt_title);
            TextView txtFooterTitle = (TextView) footer.findViewById(R.id.txt_title);
            txtHeaderTitle.setText("THE HEADER!");
            txtFooterTitle.setText("THE FOOTER!");

            mGridView.addHeaderView(header);
            mGridView.addFooterView(footer);
        }
        */

        if (mAdapter == null) {
            mAdapter = new SellAdapter(getActivity(), R.id.txt_line1);
        }

        if (mData == null) {
            mData = SampleData.generateSampleData();
        }
        mAdapter.add(sell_data);
        for (PostInfo_simple data : mData) {
            mAdapter.add(data);
        }

        mGridView.setAdapter(mAdapter);
        //mGridView.setOnScrollListener(this);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(getActivity(), position + "번", Toast.LENGTH_SHORT).show();
                if(id == 0) {
                    Intent intent = new Intent(getActivity(), SellActivity_flip.class);
                  //  intent.putExtra("product_info", id);
                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent(getActivity(), Product_Info_Activity_Sell.class);
                    intent.putExtra("product_info", id);
                    startActivity(intent);
                    Log.e("error", "1");
                }
            }
        });

    }

}