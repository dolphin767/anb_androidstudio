package com.example.anb;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anb.buy.BuyActivity;

import java.util.ArrayList;

public class PostAdapter extends ArrayAdapter<PostInfo> {

	private Context mContext;
	private int mResource;
	private ArrayList<PostInfo> mList;
	private LayoutInflater mInflater;

	public PostAdapter(Context context, int layoutResource,
			ArrayList<PostInfo> objects) {
		super(context, layoutResource, objects);
		this.mContext = context;
		this.mResource = layoutResource;
		this.mList = objects;
		this.mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PostInfo user = mList.get(position);

		if (convertView == null) {
			convertView = mInflater.inflate(mResource, null);
		}

		if (user != null) {

			TextView userName = (TextView) convertView
					.findViewById(R.id.user_id);
			TextView userTime = (TextView) convertView
					.findViewById(R.id.user_time);
			TextView userFriend = (TextView) convertView
					.findViewById(R.id.user_friend);
			TextView userWhere = (TextView) convertView
					.findViewById(R.id.user_where);
			TextView userText = (TextView) convertView
					.findViewById(R.id.user_text);
			TextView productInfo = (TextView) convertView
					.findViewById(R.id.product_info);
			ImageView userProfile = (ImageView) convertView
					.findViewById(R.id.person_profil);
			ImageView productPhoto = (ImageView) convertView
					.findViewById(R.id.product_photo);

			Button Buy = (Button) convertView.findViewById(R.id.btnBuy);
			Buy.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mContext, BuyActivity.class);
					mContext.startActivity(intent);
				}
			});
			userName.setText(user.getuserName());
			userTime.setText(user.getuserTime());
			userFriend.setText(user.getuserFriend());
			userWhere.setText(user.getuserWhere());
			userText.setText(user.getuserText());
			productInfo.setText("이름:" + user.getproductName() + " 수량"
					+ user.getproductSize() + " 가격:" + user.getproductPrice());
			userProfile.setImageBitmap(user.getuserProfile());
			productPhoto.setImageBitmap(user.getproductPhoto());
		}

		return convertView;
	}
}