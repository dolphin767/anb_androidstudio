package com.example.anb;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class MainTabsPagerAdapter extends FragmentPagerAdapter {
	String JSONString;
	public MainTabsPagerAdapter(FragmentManager fm, String JSONString) {
        super(fm);
        this.JSONString = JSONString;
    }
	
	@Override
	public Fragment getItem(int index) {
		switch(index) {
		case 0:
			Log.i("subact1","done");
			return new Buy(JSONString);
		case 1:
			Log.i("subact2","done");
			return new Sell(JSONString);
		case 2:
			Log.i("subact3","done");
			return new Present(JSONString);
		default:
			return null;
		}
	}
	
	@Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 3;
    }
}
