package com.example.anb;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;

import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.widget.LoginButton;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class FriendListActivity extends ListActivity {
    private static final String TAG = "FriendListActivity";
    private UiLifecycleHelper uiHelper;
    private LoginButton loginButton = null;
    private ArrayList<String> list;
    private ArrayAdapter<String> adapter;
    private ArrayList<Friend> m_orders;

    private void onSessionStateChange(Session session, SessionState state,
                                      Exception exception) {
        if (state.isOpened()) {
            Log.i(TAG, "Logged in...");
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
        setContentView(R.layout.activity_friend_list);
        m_orders = new ArrayList<Friend>();
        Session session = Session.getActiveSession();
        new Request(
                session,
                "/me/taggable_friends",
                null,
                HttpMethod.GET,
                new Request.Callback() {
                    public void onCompleted(Response response) {
                        GraphObject graphObject = response.getGraphObject();
                        if (graphObject != null) {
                            JSONObject jsonObject = graphObject.getInnerJSONObject();
                            String taggableFriendsJson = jsonObject.toString();
                            //Gson gson = new Gson();
                            //TaggableFriendsWrapper taggableFriendsWrapper= gson.fromJson(taggableFriendsJson, TaggableFriendsWrapper.class);
                            try {
                                JSONArray jsonArray = new JSONArray(graphObject.getProperty("data")+"");
                                for(int i=0;i<jsonArray.length();i++) {
                                    JSONObject jo = new JSONObject(jsonArray.get(i).toString());
                                    String name = jo.getString("name");
                                    String id = jo.getString("id");
                                    String picture = jo.getJSONObject("picture").getJSONObject("data").getString("url");
                                    m_orders.add(new Friend(name,id,picture));
                                }
                                FriendAdapter m_adapter = new FriendAdapter(getApplicationContext(),R.layout.row_friend,m_orders);
                                setListAdapter(m_adapter);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }else {
                            // do something
                        }
                        //response.get
                    }
                }
        ).executeAsync();
        Log.v(TAG,""+m_orders.size());
    }

    private void initializeViews() {
    }

    @Override
    public void onResume() {
        super.onResume();
        // For scenarios where the main activity is launched and user
        // session is not null, the session state change notification
        // may not be triggered. Trigger it if it's open/closed.
        Session session = Session.getActiveSession();
        if (session != null && (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }
        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_friend_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
