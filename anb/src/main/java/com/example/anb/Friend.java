package com.example.anb;

/**
 * Created by minseock on 2015-04-03.
 */
public class Friend {
    private String Name;
    private String Id;
    private String ProfilePictureSource;

    public Friend(String Name, String Id, String ProfilePictureSource) {
        this.Name = Name;
        this.Id = Id;
        this.ProfilePictureSource = ProfilePictureSource;
    }

    public String getName() {
        return this.Name;
    }
    public String getId() {
        return this.Id;
    }
    public String getProfilePictureSource() {
        return this.ProfilePictureSource;
    }
}
