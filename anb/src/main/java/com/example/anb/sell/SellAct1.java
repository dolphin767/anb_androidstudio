package com.example.anb.sell;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.anb.R;

public class SellAct1 extends Fragment implements OnClickListener {
	String JSONString;
	private ViewPager viewPager;
	private String selectedImagePath;
	private static final int SELECT_PICTURE = 1;
	ImageView myimage;
	Bitmap bt;
	SendMessage1 SM;

	public SellAct1(ViewPager viewPager, String JSONString) {
		this.viewPager = viewPager;
		this.JSONString = JSONString;
	}

	View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		v = inflater.inflate(R.layout.sell_layout_1, container, false);
		final EditText product_name = (EditText) v.findViewById(R.id.product_name);
		Button next = (Button) v.findViewById(R.id.btnSellNext);
		myimage = (ImageView) v.findViewById(R.id.imageview);
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg) {
				SM.sendData1(product_name.getText().toString());
				viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
			}
		});

		myimage.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Intent intent = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				// intent.setType("image/*");
				// intent.setAction(Intent.ACTION_GET_CONTENT);
				startActivityForResult(intent, SELECT_PICTURE);
				
			}
		});
		return v;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		this.setHasOptionsMenu(true);

	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == getActivity().RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				final Uri selectImageUri = data.getData();
				final String[] filePathColumn = { MediaStore.Images.Media.DATA };

				final Cursor imageCursor = getActivity()
						.getContentResolver()
						.query(selectImageUri, filePathColumn, null, null, null);
				imageCursor.moveToFirst();

				final int columnIndex = imageCursor
						.getColumnIndex(filePathColumn[0]);
				final String imagePath = imageCursor.getString(columnIndex);
				imageCursor.close();

				final Bitmap bitmap = BitmapFactory.decodeFile(imagePath);

				bt = bitmap.createScaledBitmap(bitmap, 150, 150, false);

				myimage.setImageBitmap(bt);
				/*
				 * ByteArrayOutputStream baos = new ByteArrayOutputStream();
				 * bt.compress(Bitmap.CompressFormat.JPEG, 100, baos); byte[] b
				 * = baos.toByteArray(); String imageEncode =
				 * Base64.encodeToString(b, Base64.DEFAULT);
				 * Log.d("endoded image", imageEncode); nameValuePairs.add(new
				 * BasicNameValuePair("image", imageEncode));
				 */
			}
		}
	}

	public String getPath(Uri uri) {
		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = getActivity().managedQuery(uri, projection, null, null,
				null);
		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	@Override
	public void onClick(View v) {

	}

	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			SM = (SendMessage1) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"You need to implement sendData method");
		}
	}

	interface SendMessage1 {
		public void sendData1(String message);

	}

}
