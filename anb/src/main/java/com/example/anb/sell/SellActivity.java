package com.example.anb.sell;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.example.anb.R;
import com.example.anb.sell.SellAct1.SendMessage1;
import com.example.anb.sell.SellAct2.SendMessage2;
import com.example.anb.sell.SellAct3.SendMessage3;

public class SellActivity extends FragmentActivity implements ActionBar.TabListener, 
SendMessage1, SendMessage2, SendMessage3 {
	private ViewPager viewPager;
	private SellTabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = {"one", "two", "three", "four"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_layout);
		Intent intent = getIntent();
		String JSONString = intent.getStringExtra("user_info");
		Log.i("js","receive on SellAct?" + JSONString);
		// Initialize.
		
		viewPager = (ViewPager)this.findViewById(R.id.pager);
		mAdapter = new SellTabsPagerAdapter(getSupportFragmentManager(), viewPager, JSONString);
		viewPager.setAdapter(mAdapter);
		viewPager.setOffscreenPageLimit(4);	// The key of all problems in universe!
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
		    @Override
		    public void onPageSelected(int position) {
		        // on changing the page
		        // make respected tab selected
		        // actionBar.setSelectedNavigationItem(position);
				Log.i("onTabSelected", "tab " + position + " selected");
		        actionBar.getTabAt(position).select();
		    }

		    @Override
		    public void onPageScrolled(int arg0, float arg1, int arg2) {
		    }

		    @Override
		    public void onPageScrollStateChanged(int arg0) {
		    }
		});
		
		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setIcon(null)
								.setTabListener(this));
		}
	
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState)	{
		super.onSaveInstanceState(outState);
		outState.putInt("selected Tab ", getActionBar().getSelectedNavigationIndex());
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
        // show respected fragment view
        viewPager.setCurrentItem(tab.getPosition());
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig)	{
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void sendData1(String name) {
		// TODO Auto-generated method stub
		SellAct4 result = (SellAct4) getSupportFragmentManager().findFragmentById(
				R.id.pager);
		result.getData1(name);
		
	}
	@Override
	public void sendData2(String size, String amount, String price) {
		// TODO Auto-generated method stub
		SellAct4 result = (SellAct4) getSupportFragmentManager().findFragmentById(
				R.id.pager);
		result.getData2(size, amount, price);
		
	}
	@Override
	public void sendData3(String delivery, String direct, String visit, String address) {
		// TODO Auto-generated method stub
		SellAct4 result = (SellAct4) getSupportFragmentManager().findFragmentById(
				R.id.pager);
		result.getData3(delivery, direct,  visit, address);
		
	}

	
}
