package com.example.anb.sell;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.anb.R;

public class SellAct3 extends Fragment {
	private ViewPager viewPager;
	SendMessage3 SM;
	String JSONString;
	String delivery, direct, visit, address;
	public SellAct3(ViewPager viewPager, String JSONString) {
		this.viewPager = viewPager;
		this.JSONString = JSONString;
	}
	
	View v;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		v = inflater.inflate(R.layout.sell_layout_3, container, false);
		Button next = (Button) v.findViewById(R.id.btnSellNext);
		final CheckBox del = (CheckBox) v.findViewById(R.id.sellCheckBox1);
		final CheckBox dir = (CheckBox) v.findViewById(R.id.sellCheckBox2);
		final CheckBox vis = (CheckBox) v.findViewById(R.id.sellCheckBox3);
		final EditText add = (EditText) v.findViewById(R.id.sell_address);
		
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg) {
				if(del.isChecked()) delivery = "true";
				else delivery = "false";
				if(dir.isChecked()) direct = "true";
				else direct = "false";
				if(vis.isChecked()) {
					visit = "true";
					address = add.getText().toString();
				}
				else {
					visit = "false";
					address = "";
				}
				SM.sendData3(delivery, direct, visit, address);
				viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
			}
		});
		
		return v;

	}
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			SM = (SendMessage3) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"You need to implement sendData method");
		}
	}
	interface SendMessage3 {
		public void sendData3(String delivery, String direct, String visit, String address);
		
	}

}
