package com.example.anb.sell;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.format.Time;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.anb.Post;
import com.example.anb.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class SellAct4 extends Fragment {
	private ViewPager viewPager;
	String URL = "http://54.178.153.85:3000/main/insert";
	String JSONString;
	String product_name;
	String size;
	int amount = 0;
	int price = 0;
	String delivery;
	String direct;
	String visit;
	String address;
	String user_name;

	public SellAct4(ViewPager viewPager, String JSONString) {
		this.viewPager = viewPager;
		this.JSONString = JSONString;
	}

	public void getData1(String name) {
		this.product_name = name;
	}

	public void getData2(String size, String amount, String price) {
		this.size = size;
		if (amount.length() == 0)
			this.amount = 0;
		else
			this.amount = Integer.parseInt(amount);
		if (price.length() == 0)
			this.price = 0;
		else
			this.price = Integer.parseInt(price);
	}

	public void getData3(String delivery, String direct, String visit,
			String address) {
		this.delivery = delivery;
		this.direct = direct;
		this.visit = visit;
		this.address = address;
	}

	View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i("js", "this is " + JSONString);
		JSONObject for_user;
		try {
			for_user = new JSONObject(JSONString);
			user_name = for_user.getString("name");
		} catch (JSONException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		v = inflater.inflate(R.layout.sell_layout_4, container, false);
		final EditText text = (EditText) v.findViewById(R.id.sell_comment);
		Button next = (Button) v.findViewById(R.id.btnSellNext);
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg) {
				Toast.makeText(getActivity(), "글을 게시했습니다.", Toast.LENGTH_SHORT)
						.show();
				String id_num = "";
				String result = "";
				String comment = text.getText().toString();
				// Log.i("js", "" + JSONString);
				ArrayList<String> postInfo = new ArrayList<String>();
				try {
					JSONObject received = new JSONObject(JSONString);
					id_num = received.get("_id").toString();
				} catch (JSONException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				JSONObject jo = new JSONObject();

				//
				Bitmap nullImage = BitmapFactory.decodeResource(getActivity()
						.getResources(), R.drawable.null_image);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				nullImage.compress(Bitmap.CompressFormat.JPEG, 100, baos);
				byte[] b = baos.toByteArray();
				String imageEncode = Base64.encodeToString(b, Base64.DEFAULT);
				//

				try {
					Time now = new Time();
					now.setToNow();
					Log.i("js", "id_ = " + id_num);
					jo.put("id_num", id_num);
					jo.put("name", user_name);
					jo.put("product_name", product_name);
					jo.put("size", size);
					jo.put("amount", amount);
					jo.put("price", price);
					jo.put("delivery", delivery);
					jo.put("direct", direct);
					jo.put("visit", visit);
					jo.put("address", address);
					jo.put("description", comment);
					jo.put("time", now.format("%Y년 %m월 %d일 - %H:%M"));
					// jo.put("product_photo", imageEncode);
					Log.i("js", "posted " + jo.toString());
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					// Log.i("js", "send " + b_num);
					postInfo.add(URL);
					postInfo.add(jo.toString());
					Post post = new Post();
					result = post.execute(postInfo).get();
				} catch (InterruptedException | ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				getActivity().finish();
			}
		});

		return v;

	}

}
