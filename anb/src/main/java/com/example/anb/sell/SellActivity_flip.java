package com.example.anb.sell;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.example.anb.Post;
import com.example.anb.R;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by edward on 2015-07-14.
 */
public class SellActivity_flip extends Activity implements View.OnClickListener {

    private static final int SELECT_PICTURE = 1;
    ImageView myimage;

    String URL = "http://54.178.153.85:3000/main/insert";
    String JSONString;
    String product_name;
    String size;
    int amount = 0;
    int price = 0;
    String delivery;
    String direct;
    String visit;
    String address;
    String user_name;
    final int MAX = 4;
    Button btnPrev, btnNext;
    ViewFlipper vf;
    int cnt = 0;
    Bitmap bt;
    Animation slide_in_left, slide_out_right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sell_layout);
        Intent intent = getIntent();
        intent.getExtras();
        btnPrev = (Button) findViewById(R.id.prev);
        btnNext = (Button) findViewById(R.id.next);
        vf = (ViewFlipper) findViewById(R.id.viewflipper);
        final EditText product_name = (EditText)findViewById(R.id.product_name);
        Button next = (Button) findViewById(R.id.btnSellNext);
        myimage = (ImageView) findViewById(R.id.imageview);
        myimage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final Intent intent = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, SELECT_PICTURE);

            }
        });

        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.prev:
                if(cnt > 0) {
                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt--;
                    vf.showPrevious();

                }
                else
                    finish();
                break;
            case R.id.next:
                if(cnt < MAX-1) {

                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt++;
                    vf.showNext();
                }
                else {
                    String result;
                    try {
                        final EditText size = (EditText) findViewById(R.id.product_size);
                        final EditText amount = (EditText) findViewById(R.id.product_amount);
                        final EditText price = (EditText) findViewById(R.id.product_price);
                        final CheckBox del = (CheckBox) findViewById(R.id.sellCheckBox1);
                        final CheckBox dir = (CheckBox) findViewById(R.id.sellCheckBox2);
                        final CheckBox vis = (CheckBox) findViewById(R.id.sellCheckBox3);
                        final EditText add = (EditText) findViewById(R.id.sell_address);
                        if(del.isChecked()) delivery = "true";
                        else delivery = "false";
                        if(dir.isChecked()) direct = "true";
                        else direct = "false";
                        if(vis.isChecked()) {
                            visit = "true";
                            address = add.getText().toString();
                        }
                        else {
                            visit = "false";
                            address = "";
                        }
                        ArrayList<String> postInfo = new ArrayList<String>();
                        postInfo.add(URL);


                        Post post = new Post();
                        result = post.execute(postInfo).get();

                        finish();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        //Log.i("js", "2");
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        // TODO Auto-generated catch block
                        //Log.i("js", "3");
                        e.printStackTrace();
                    }


                    finish();
                }
                break;

            default:
                break;
        }

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == this.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                final Uri selectImageUri = data.getData();
                final String[] filePathColumn = { MediaStore.Images.Media.DATA };

                final Cursor imageCursor = this
                        .getContentResolver()
                        .query(selectImageUri, filePathColumn, null, null, null);
                imageCursor.moveToFirst();

                final int columnIndex = imageCursor
                        .getColumnIndex(filePathColumn[0]);
                final String imagePath = imageCursor.getString(columnIndex);
                imageCursor.close();

                final Bitmap bitmap = BitmapFactory.decodeFile(imagePath);

                bt = bitmap.createScaledBitmap(bitmap, 150, 150, false);

                myimage.setImageBitmap(bt);

            }
        }
    }



}
