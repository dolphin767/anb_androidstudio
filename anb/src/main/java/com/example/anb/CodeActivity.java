package com.example.anb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by edward on 2015-07-10.
 */
public class CodeActivity extends Activity {
    String JSONString;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        //JSONString = intent.getStringExtra("user_info");
        setContentView(R.layout.code_layout);
        Button button = (Button) findViewById(R.id.code_btn);
        final EditText editText = (EditText) findViewById(R.id.codeText);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "code:" + editText.getText().toString(), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
