package com.example.anb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class TradeAdapter extends ArrayAdapter<TradeInfo> {

	private Context mContext;
	private int mResource;
	private ArrayList<TradeInfo> mList;
	private LayoutInflater mInflater;

	public TradeAdapter(Context context, int layoutResource,
			ArrayList<TradeInfo> objects) {
		super(context, layoutResource, objects);
		this.mContext = context;
		this.mResource = layoutResource;
		this.mList = objects;
		this.mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TradeInfo user = mList.get(position);

		if (convertView == null) {
			convertView = mInflater.inflate(mResource, null);
		}

		if (user != null) {
			
			TextView tradeText = (TextView) convertView
					.findViewById(R.id.trade_info);

			switch(user.getStatus()){
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			}
			if (user.getStatus()==1) {
				tradeText.setText(user.getName() + "sells" + user.getproductName()
						+ " " + user.getproductSize() + "kg as one");
			} else {
				tradeText.setText(user.getName() + "buys" + user.getproductName()
						+ " " + user.getproductSize() + "kg as one");
			
			}
		}

		return convertView;
	}
}