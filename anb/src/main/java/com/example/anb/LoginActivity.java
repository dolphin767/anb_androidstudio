package com.example.anb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

//import android.util.Log;

public class LoginActivity extends Activity {
	static String emailInput;

	/** Called when linked to login activity */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login_activity);

		final EditText id = (EditText) findViewById(R.id.idLogin);
		final EditText password = (EditText) findViewById(R.id.passLogin);
		Button loginButton = (Button) findViewById(R.id.btnLogin);
		TextView id_pw_find = (TextView) findViewById(R.id.link_to_id_pw_find);

		loginButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				emailInput = id.getText().toString();
				String passInput = password.getText().toString();
				Intent intent = new Intent(LoginActivity.this,
						MainActivity.class);
				//startActivity(intent);
				
				try {
					String JSONString = null;
					String URL = "http://54.178.153.85:3000/users/login"; // server
																	// URL
					ArrayList<String> postInfo = new ArrayList<String>();
					postInfo.add(URL);
					postInfo.add(emailInput);
					postInfo.add(passInput);
					Post post = new Post();
					//JSONString = post.execute(postInfo).get();

					//JSONObject jo = new JSONObject(JSONString);
//					if (jo.length() == 0) {
//						Toast.makeText(LoginActivity.this, "Login Failed!",
//								Toast.LENGTH_SHORT).show();
//						startActivity(intent);
//						finish();
//					} else {
						
						//intent.putExtra("user_info", JSONString);

						startActivity(intent);
						finish();
//					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		id_pw_find.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(LoginActivity.this,
						Id_Pw_Activity.class);

				startActivity(intent);

			}
		});
	}
}