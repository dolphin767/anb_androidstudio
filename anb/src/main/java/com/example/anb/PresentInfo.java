package com.example.anb;

import android.graphics.Bitmap;

public class PresentInfo {
	private Bitmap presentPhoto;
	private Boolean receive;
	private String name;
	private String eventTime;
	private String productName;
	private String productSize;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getSort() {
		return receive;
	}
	public void setSort(Boolean bool) {
		this.receive = bool;
	}
	public String getproductSize() {
		return productSize;
	}
	public void setproductSize(String productSize) {
		this.productSize = productSize;
	}
	public String getproductName() {
		return productName;
	}
	public void setproductName(String productName) {
		this.productName = productName;
	}
	public Bitmap getpresentPhoto() {
		return presentPhoto;
	}
	public void setpresentPhoto(Bitmap presentPhoto) {
		this.presentPhoto = presentPhoto;
	}
	public String geteventTime() {
		return eventTime;
	}
	public void seteventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
}
