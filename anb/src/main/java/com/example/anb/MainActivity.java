package com.example.anb;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.anb.navidrawer.FriendPage;
import com.example.anb.navidrawer.MyPage;
import com.example.anb.navidrawer.SettingPage;
import com.example.anb.navidrawer.TradeManagePage;
import com.example.anb.test.ExpandActivity;
import com.example.anb.test.ViewFlipperActivity;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {
    private ActionBarDrawerToggle dtToggle;
    private ViewPager viewPager;
	private MainTabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = {"Buy", "Sell", "Present"};
    private String[] navigation = {"My page", "Friend list", "Setting", "Trade list", "Expand"};

	//preference
	private SharedPreferences mPref;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPref = PreferenceManager.getDefaultSharedPreferences(this); //preference
		//getPreferencesData();
		setContentView(R.layout.fragment_layout);
		Intent intent = getIntent();
		String JSONString = intent.getStringExtra("user_info");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActionBar().getThemedContext(), android.R.layout.simple_list_item_1, navigation);
        final DrawerLayout drawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        final ListView navList = (ListView) findViewById(R.id.drawer);

        viewPager = (ViewPager) this.findViewById(R.id.pager);
        mAdapter = new MainTabsPagerAdapter(getSupportFragmentManager(), JSONString);
        viewPager.setAdapter(mAdapter);

        viewPager.setOffscreenPageLimit(3); // The key of all problems in
        // universe!

        dtToggle = new ActionBarDrawerToggle(this, drawer, R.drawable.ic_drawer,R.string.open_drawer ,R.string.close_drawer){
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

        };
        drawer.setDrawerListener(dtToggle);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        navList.setAdapter(adapter);
        navList.setOnItemClickListener(new OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int pos,long id){
                drawer.setDrawerListener( new DrawerLayout.SimpleDrawerListener(){

                    @Override
                    public void onDrawerClosed(View drawerView){
                        super.onDrawerClosed(drawerView);
                       //창띄우기
						Toast.makeText(MainActivity.this, "click"+pos, Toast.LENGTH_SHORT).show();
						Intent intent;
						switch(pos){
							case 0:
								intent = new Intent(MainActivity.this, MyPage.class);
								startActivity(intent);
								break;
							case 1:
								intent = new Intent(MainActivity.this, FriendPage.class);
								startActivity(intent);
								break;
							case 2:
								intent = new Intent(MainActivity.this, SettingPage.class);
								startActivity(intent);
								break;
							case 3:
								intent = new Intent(MainActivity.this, TradeManagePage.class);
								startActivity(intent);
								break;
							case 4:
								intent = new Intent(MainActivity.this, ExpandActivity.class);
								startActivity(intent);
								break;

							default:
								break;
						}
                    }
                });
                drawer.closeDrawer(navList);
            }
        });
        // Initialize.

		/**
		 * Swiping the viewPager make respective tab selected.
		 **/
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				// actionBar.setSelectedNavigationItem(position);
				Log.i("onTabSelected", "tab " + position + " selected");
				actionBar.getTabAt(position).select();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);

		// Add tabs.

		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main_activity_actions, menu);
	    return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("selected Tab ", getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		viewPager.setCurrentItem(tab.getPosition());
	}

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        dtToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(dtToggle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        dtToggle.syncState();
    }
	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

}
