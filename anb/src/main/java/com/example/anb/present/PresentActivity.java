package com.example.anb.present;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.anb.Post;
import com.example.anb.R;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by edward on 2015-07-14.
 */
public class PresentActivity extends Activity implements View.OnClickListener {
    String JSONString;
    public PresentActivity(String JSONString) {
        this.JSONString = JSONString;
    }
    final int MAX = 4;
    Button btnPrev, btnNext;
    ViewFlipper vf;
    int cnt = 0;
    Animation slide_in_left, slide_out_right;
    ImageView profile;
    private static final int SELECT_PICTURE = 1;

    String URL = "http://54.178.153.85:3000/users/join";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.present_info);

        btnPrev = (Button) findViewById(R.id.prev);
        btnNext = (Button) findViewById(R.id.next);
        vf = (ViewFlipper) findViewById(R.id.viewflipper);


        RadioGroup rd = (RadioGroup)findViewById(R.id.radiogroup1);
        rd.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()            {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int selectedId) {
                switch (selectedId) {
                    case R.id.radio1:

                        break;
                    case R.id.radio2:

                        break;

                }

            }
        });
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.prev:
                if(cnt > 0) {
                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt--;
                    vf.showPrevious();

                }
                else
                    finish();
                break;
            case R.id.next:
                if(cnt < MAX-1) {

                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt++;
                    vf.showNext();
                }
                else {
                        String result;
                        try {

                            ArrayList<String> postInfo = new ArrayList<String>();
                            postInfo.add(URL);


                            Post post = new Post();
                            result = post.execute(postInfo).get();

                            finish();
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            //Log.i("js", "2");
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            // TODO Auto-generated catch block
                            //Log.i("js", "3");
                            e.printStackTrace();
                        }


                           finish();
                }
                break;

            default:
                break;
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        Toast.makeText(getBaseContext(), "resultCode : "+resultCode,Toast.LENGTH_SHORT).show();

        if(requestCode == SELECT_PICTURE)
        {
            if(resultCode==Activity.RESULT_OK)
            {
                try {
                    //Uri에서 이미지 이름을 얻어온다.
                    //String name_Str = getImageNameToUri(data.getData());

                    //이미지 데이터를 비트맵으로 받아온다.
                    Bitmap image_bitmap 	= MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    ImageView profile = (ImageView)findViewById(R.id.person_profil);
                    //배치해놓은 ImageView에 set
                    profile.setImageBitmap(image_bitmap);


                    //Toast.makeText(getBaseContext(), "name_Str : "+name_Str , Toast.LENGTH_SHORT).show();


                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }



}
