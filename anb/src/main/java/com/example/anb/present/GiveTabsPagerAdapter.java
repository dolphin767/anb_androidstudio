package com.example.anb.present;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

public class GiveTabsPagerAdapter extends FragmentPagerAdapter {
	ViewPager viewPager;
	String JSONString;
	public GiveTabsPagerAdapter(FragmentManager fm,ViewPager viewPager, String JSONString) {
        super(fm);
        this.viewPager=viewPager;
        this.JSONString = JSONString;
    }
	
	
	@Override
	public Fragment getItem(int index) {
		switch(index) {
		case 0:
			Log.i("subact1","done");
			return new GiveAct1(viewPager, JSONString);
		case 1:
			Log.i("subact2","done");
			return new GiveAct2(viewPager, JSONString);
		case 2:
			Log.i("subact3","done");
			return new GiveAct3(viewPager, JSONString);
		case 3:
			Log.i("subact4","done");
			return new GiveAct4(viewPager, JSONString);
		default:
			return null;
		}
	}
	
	@Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }
}
