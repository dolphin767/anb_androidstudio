package com.example.anb.present;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.anb.R;

public class GiveAct2 extends Fragment {
	private ViewPager viewPager;
	String JSONString;
	SendMessage2 SM;
	public GiveAct2(ViewPager viewPager, String JSONString) {
		this.viewPager = viewPager;
		this.JSONString = JSONString;
	}

	View v;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		v = inflater.inflate(R.layout.sell_layout_2, container, false);
		Button next = (Button) v.findViewById(R.id.btnSellNext);
		final EditText size = (EditText) v.findViewById(R.id.product_size);
		final EditText amount = (EditText) v.findViewById(R.id.product_amount);
		final EditText price = (EditText) v.findViewById(R.id.product_price);
		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg) {
				SM.sendData2(size.getText().toString(), amount.getText().toString(), price.getText().toString());
				viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
			}
		});
		
		return v;

	}
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			SM = (SendMessage2) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"You need to implement sendData method");
		}
	}
	interface SendMessage2 {
		public void sendData2(String size, String amount, String price);
		
	}
}
