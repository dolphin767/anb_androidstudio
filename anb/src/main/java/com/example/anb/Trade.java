package com.example.anb;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class Trade extends Fragment {
	private ArrayList<TradeInfo> tradeList;
	private TradeAdapter pAdapter;
	private ListView myList;
	final int maxTradeNumber = 5;
	final int MAX_PRESENT = 1000;
	View v;
	private ProgressDialog pDialog;
	String JSONString;

	public Trade(String JSONString) {
		this.JSONString = JSONString;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.tradelist, container, false);
		String[] showlist = getResources().getStringArray(R.array.tradeArray);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,showlist);
		adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		tradeList = new ArrayList<TradeInfo>();
		Spinner mode_select = (Spinner)v.findViewById(R.id.trade_spinner);
		mode_select.setAdapter(adapter);
		mode_select.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedView, int position, long id){
				Spinner sp = (Spinner)v.findViewById(R.id.trade_spinner);
				String resultText = "";
				if(sp.getSelectedItemPosition()>=0){
					resultText=(String)sp.getAdapter().getItem(sp.getSelectedItemPosition());
				
				}
			}
			public void onNothingSelected(AdapterView<?> parentView){
				
			}
			
		});
		myList = (ListView) v.findViewById(R.id.trade_listView);
		pAdapter = new TradeAdapter(getActivity(), R.layout.trade_info, tradeList);
		myList.setAdapter(pAdapter);
		
		ArrayList<String> postInfo = new ArrayList<String>();
		ArrayList<String> postInfo2 = new ArrayList<String>();

		
		String[] pid = new String[5];
		String[] anb = new String[5];
		int array_len = 0;
		String result = "";
		String result2 = "";
		
				
		Log.e("js","wall");
		setTradeList("test", "give");
		setTradeList("test", "take");
		setTradeList("test", "give");
		setTradeList("test", "give");
		setTradeList("test", "take");
		try {
			JSONArray ja3 = new JSONArray(result2);
			for (int i = array_len - 1; i >= 0; i--) {
				setTradeList(ja3.getJSONObject(i).toString(), anb[i]);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	private void setTradeList(String result, String anb) {
		
				
		//try {
			//JSONObject jsonobject = new JSONObject(result);
			TradeInfo tradeItem = new TradeInfo();

			tradeItem.setName("jaesung");
			tradeItem.setproductName("jaesung's apple");
			tradeItem.setproductSize("3");
			tradeItem.setStatus(1);
			
			tradeList.add(tradeItem);

			pAdapter.notifyDataSetChanged();
		//} catch (JSONException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}

	}

}