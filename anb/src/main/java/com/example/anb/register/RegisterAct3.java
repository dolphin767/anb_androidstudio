package com.example.anb.register;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.anb.Post;
import com.example.anb.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class RegisterAct3 extends Fragment implements OnClickListener {
	private ViewPager viewPager;
	View v;
	String name = "damn";
	String email = "shit@never.com";
	String password = "shit";
	String password_check = "shit";
	String gender = "male";
	String URL = "http://54.178.153.85:3000/users/join";

	public RegisterAct3(ViewPager viewPager) {
		this.viewPager = viewPager;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.register_layout_3, container, false);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		this.setHasOptionsMenu(true);
		final EditText emailText = (EditText) v.findViewById(R.id.reg_email);
		final EditText passwordText = (EditText) v
				.findViewById(R.id.reg_password);
		final EditText password_checkText = (EditText) v
				.findViewById(R.id.reg_password_check);
		Button complete = (Button) v.findViewById(R.id.btnRegisterComplete);

		complete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg) {

				email = emailText.getText().toString();
				password = passwordText.getText().toString();
				password_check = password_checkText.getText().toString();

				if (!password.equals(password_check)) {
					Toast.makeText(getActivity(),
							"Password not correct", Toast.LENGTH_SHORT)
							.show();
				} else if (email.equals("") && password.equals("")) {
					Toast.makeText(getActivity(), "Register complete",
							Toast.LENGTH_SHORT).show();
				} else {
					Log.i("js", "name email pw : " + name + email + password);
					String result;
					JSONObject jo = new JSONObject();
					try {
						jo.put("name", name);
						jo.put("email", email);
						jo.put("password", password);
						jo.put("gender", gender);
						String JSONString = null;
						String URL = "http://54.178.153.85:3000/users/join"; // server
						// URL

						ArrayList<String> postInfo = new ArrayList<String>();
						postInfo.add(URL);
						postInfo.add(email);
						postInfo.add(password);
						postInfo.add(name);
						postInfo.add(gender);

						Post post = new Post();
						result = post.execute(postInfo).get();
						Toast.makeText(getActivity(), "register complete",
								Toast.LENGTH_SHORT).show();
						Log.i("js result", result);

						getActivity().finish();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
                        //Log.i("js", "1");
						e.printStackTrace();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
                        //Log.i("js", "2");
						e.printStackTrace();
					} catch (ExecutionException e) {
						// TODO Auto-generated catch block
                        //Log.i("js", "3");
						e.printStackTrace();
					}

					/*
					 * try { String JSONString = null;
					 * 
					 * JSONString = new doRegister().execute( email, password,
					 * name).get(); while (JSONString == null) { } if
					 * (JSONString.isEmpty()) { Toast.makeText(getActivity(),
					 * "Server connection failed", Toast.LENGTH_SHORT).show(); }
					 * else { JSONObject resultObject = new
					 * JSONObject(JSONString); String result =
					 * resultObject.getString("result"); if
					 * (result.equals("failed")) { String errorMessage =
					 * resultObject .getString("error");
					 * Toast.makeText(getActivity(), errorMessage,
					 * Toast.LENGTH_SHORT).show(); } else {
					 * Toast.makeText(getActivity(), "Register Complete!",
					 * Toast.LENGTH_SHORT).show(); Intent intent = new
					 * Intent(getActivity(), MainActivity.class);
					 * startActivity(intent); // Register successed. intro
					 * activity is no use; // finish it! getActivity().finish();
					 * } } } catch (Exception e) { e.printStackTrace(); }
					 */
				}
			}
		});

	}

	@Override
	public void onClick(View v) {

	}

	public void getData(String message) {
		name = message;
		Log.i("js", "name at register is " + message);
	}
	public void getData2(String message) {
		gender = message;
		Log.i("js", "gender at register is " + message);
	}



}
