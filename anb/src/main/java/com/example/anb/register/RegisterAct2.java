package com.example.anb.register;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.anb.R;

public class RegisterAct2 extends Fragment implements RadioGroup.OnCheckedChangeListener, OnClickListener {
    private ViewPager viewPager;

    public RegisterAct2(ViewPager viewPager) {
        this.viewPager = viewPager;
    }

    String gender = "male";
    SendMessage2 SM;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.register_layout_2, container, false);
        Button next = (Button) v.findViewById(R.id.btnRegNext);

        RadioGroup rd = (RadioGroup) v.findViewById(R.id.radiogroup1);
        rd.setOnCheckedChangeListener(this); // 라디오버튼을 눌렸을때의 반응
        next.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg) {
               viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                Log.e("gender", gender);
                SM.sendData2(gender);
                // how to communicate with other fragments?
            }

        });

        return v;

    }

    public void onCheckedChanged(RadioGroup arg0, int arg1) { // 라디오버튼
        // TODO Auto-generated method stub
        switch (arg1) {
            case R.id.radio1:
                gender = "male";
                break;
            case R.id.radio2:
                gender = "female";
                break;

        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Log.e("reg_2", "success");
        this.setHasOptionsMenu(true);

    }
        @Override
        public void onAttach (Activity activity){
            super.onAttach(activity);
            try {
                SM = (SendMessage2) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(
                        "You need to implement sendData method");
            }
        }
        interface SendMessage2 {
            public void sendData2(String gender);

        }

        @Override
        public void onClick (View v){
            // TODO Auto-generated method stub

        }

    }

