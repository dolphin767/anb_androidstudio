package com.example.anb.register;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.example.anb.R;
import com.example.anb.register.RegisterAct1.SendMessage;

public class RegisterActivity extends FragmentActivity implements
		ActionBar.TabListener, SendMessage, RegisterAct2.SendMessage2 {
	private ViewPager viewPager;
	private RegisterTabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = { "one", "two", "three" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_layout);

		// Initialize.
		viewPager = (ViewPager) this.findViewById(R.id.pager);
		mAdapter = new RegisterTabsPagerAdapter(getSupportFragmentManager(),
				viewPager);
		viewPager.setAdapter(mAdapter);

		viewPager.setOffscreenPageLimit(3); // The key of all problems in
											// universe!

		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				// actionBar.setSelectedNavigationItem(position);
				Log.i("onTabSelected", "tab " + position + " selected");
				actionBar.getTabAt(position).select();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayShowHomeEnabled(false);

		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setIcon(null)
					.setTabListener(this));

		}
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("selected Tab ", getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void sendData(String message) {
		// TODO Auto-generated method stub
		RegisterAct3 f3 = (RegisterAct3) getSupportFragmentManager().findFragmentById(
				R.id.pager);
		f3.getData(message);
	}
	public void sendData2(String gender) {
		// TODO Auto-generated method stub
		RegisterAct3 f3 = (RegisterAct3) getSupportFragmentManager().findFragmentById(
				R.id.pager);
		f3.getData2(gender);
	}

	@Override
	public void onTabSelected(Tab tab, android.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabUnselected(Tab tab, android.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, android.app.FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

}
