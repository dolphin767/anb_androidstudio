package com.example.anb.register;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.anb.R;

public class RegisterAct1 extends Fragment implements OnClickListener {
	private ViewPager viewPager;

	public RegisterAct1(ViewPager viewPager) {
		this.viewPager = viewPager;
	}

	SendMessage SM;
	View v;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		v = inflater.inflate(R.layout.register_layout_1, container, false);
		Button next = (Button) v.findViewById(R.id.btnRegNext);

		next.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg) {
				EditText name = (EditText) v.findViewById(R.id.reg_name);

				viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
				Log.e("name", name.getText().toString());
				SM.sendData(name.getText().toString());
				// how to communicate with other fragments?
			}

		});

		return v;

	}



	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		Log.e("reg_1", "success");
		this.setHasOptionsMenu(true);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			SM = (SendMessage) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(
					"You need to implement sendData method");
		}
	}
	interface SendMessage {
		public void sendData(String message);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

	}

}
