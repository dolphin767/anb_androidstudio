package com.example.anb.register;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.anb.Post;
import com.example.anb.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


/**
 * Created by edward on 2015-07-14.
 */
public class RegActivity extends Activity implements View.OnClickListener {
    final int MAX = 3;
    Button btnPrev, btnNext;
    ViewFlipper vf;
   int cnt = 0;
    Animation slide_in_left, slide_out_right;
    String gender ="male";
    String user_name = "damn";
    String user_email = "shit@never.com";
    String user_password = "shit";
    String user_password_check = "shit";
    String user_gender = "male";
    String URL = "http://54.178.153.85:3000/users/join";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);

        btnPrev = (Button) findViewById(R.id.prev);
        btnNext = (Button) findViewById(R.id.next);
        vf = (ViewFlipper) findViewById(R.id.viewflipper);


        RadioGroup rd = (RadioGroup)findViewById(R.id.radiogroup1);
       rd.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()            {

            @Override
            public void onCheckedChanged(RadioGroup arg0, int selectedId) {
                switch (selectedId) {
                    case R.id.radio1:
                        gender = "male";
                        break;
                    case R.id.radio2:
                        gender = "female";
                        break;

                }

            }
        });
        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.prev:
                if(cnt > 0) {
                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt--;
                    vf.showPrevious();

                }
                else
                    finish();
                break;
            case R.id.next:
                if(cnt < MAX-1) {

                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt++;
                    vf.showNext();
                }
                else {
                    EditText name = (EditText) findViewById(R.id.reg_name);
                    EditText emailText = (EditText) findViewById(R.id.reg_email);
                    EditText passwordText = (EditText) findViewById(R.id.reg_password);
                    EditText password_checkText = (EditText) findViewById(R.id.reg_password_check);
                    user_name = name.getText().toString();
                    user_gender = gender;
                    user_email = emailText.getText().toString();
                    user_password = passwordText.getText().toString();
                    user_password_check = password_checkText.getText().toString();

                    if (!user_password.equals(user_password_check)) {
                        Toast.makeText(this,
                                "Password not correct", Toast.LENGTH_SHORT)
                                .show();
                    } else if (user_email.equals("") && user_password.equals("")) {
                        Toast.makeText(this, "Register complete",
                                Toast.LENGTH_SHORT).show();
                    } else {

                        String result;
                        try {

                            ArrayList<String> postInfo = new ArrayList<String>();
                            postInfo.add(URL);
                            postInfo.add(user_email);
                            postInfo.add(user_password);
                            postInfo.add(user_name);
                            postInfo.add(user_gender);

                            Post post = new Post();
                            result = post.execute(postInfo).get();
                           Toast.makeText(this, "register complete",
                                    Toast.LENGTH_SHORT).show();
                            Log.i("js result", result);
                            Toast.makeText(this, "ans"+user_name + user_email + user_password, Toast.LENGTH_LONG).show();
                           finish();
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            //Log.i("js", "2");
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            // TODO Auto-generated catch block
                            //Log.i("js", "3");
                            e.printStackTrace();
                        }


                    }

                    finish();
                }
                break;

            default:
                break;
        }

    }


}
