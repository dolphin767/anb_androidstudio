package com.example.anb;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.anb.register.RegActivity;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

//54.178.153.85
//import android.util.Log;

public class IntroActivity extends Activity {
	private static final String TAG = "MainFragment";
	private UiLifecycleHelper uiHelper;
	private LoginButton loginButton = null;


	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Logged in...");
			Intent intent = new Intent(IntroActivity.this,
					LoginActivity.class);
			startActivity(intent);
		} else if (state.isClosed()) {
			Log.i(TAG, "Logged out...");
		}
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 requestWindowFeature(Window.FEATURE_NO_TITLE);
		uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		setContentView(R.layout.intro_activity);
		initializeViews();
		Button loginButton2 = (Button) findViewById(R.id.btnLogin);
		showHashKey(getApplicationContext());

		loginButton2.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(IntroActivity.this,
						LoginActivity.class);
				startActivity(intent);
			}
		});
		Button registerView = (Button) findViewById(R.id.link_to_register);
	    registerView.setOnClickListener(new View.OnClickListener() {
		 @Override
		 public void onClick(View v) {

		 Intent intent = new Intent(IntroActivity.this,
		 RegActivity.class);

		 startActivity(intent);

		 }
		 });
	}

	private void initializeViews() {

		loginButton = (LoginButton) findViewById(R.id.authButton);
        loginButton.setReadPermissions("user_friends");
        loginButton.setReadPermissions("read_custom_friendlists");
		loginButton
				.setUserInfoChangedCallback(new LoginButton.UserInfoChangedCallback() {
					@Override
					public void onUserInfoFetched(GraphUser user) {
						Log.v("user", user + "");
					}
				});

	}

	@Override
	public void onResume() {
		super.onResume();
		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	public static void showHashKey(Context context) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(
					"com.example.anb", PackageManager.GET_SIGNATURES); // Your
																		// package
																		// name
																		// here
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
               // Toast.makeText(IntroActivity.this
               //         , Base64.encodeToString(md.digest(), Base64.DEFAULT),Toast.LENGTH_LONG).show();
                Log.e("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}
	}

	/** Called when the activity is first created. */
	// @Override
	// public void onCreate(Bundle savedInstanceState) {
	// super.onCreate(savedInstanceState);
	// setContentView(R.layout.intro_activity);
	//
	// Button loginButton = (Button) findViewById(R.id.btnLogin);
	// Button registerView = (Button) findViewById(R.id.link_to_register);
	// //Button facebookRegisterView = (Button)
	// findViewById(R.id.link_to_facebook_register);
	//
	// loginButton.setOnClickListener(new View.OnClickListener() {
	// @Override
	// public void onClick(View v) {

	// }
	// });
	//
	// registerView.setOnClickListener(new View.OnClickListener() {
	// @Override
	// public void onClick(View v) {
	//
	// Intent intent = new Intent(IntroActivity.this,
	// RegisterActivity.class);
	//
	// startActivity(intent);
	//
	// }
	// });
	// }

}