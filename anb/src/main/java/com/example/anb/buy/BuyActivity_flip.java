package com.example.anb.buy;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.example.anb.Post;
import com.example.anb.R;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by edward on 2015-07-14.
 */
public class BuyActivity_flip extends Activity implements View.OnClickListener {

    final int MAX = 4;
    Button btnPrev, btnNext;
    ViewFlipper vf;
    int cnt = 0;
    Bitmap bt;
    Animation slide_in_left, slide_out_right;
    String URL = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buy_layout);
        Intent intent = getIntent();
        intent.getExtras();

        btnPrev = (Button) findViewById(R.id.prev);
        btnNext = (Button) findViewById(R.id.next);
        vf = (ViewFlipper) findViewById(R.id.viewflipper);
        final EditText product_name = (EditText)findViewById(R.id.product_name);
        Button next = (Button) findViewById(R.id.btnSellNext);
        ImageView myimage = (ImageView) findViewById(R.id.imageview);

        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.prev:
                if(cnt > 0) {
                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt--;
                    vf.showPrevious();

                }
                else
                    finish();
                break;
            case R.id.next:
                if(cnt < MAX-1) {

                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt++;
                    vf.showNext();
                }
                else {
                    String result;
                    try {

                        ArrayList<String> postInfo = new ArrayList<String>();
                        postInfo.add(URL);


                        Post post = new Post();
                        result = post.execute(postInfo).get();

                        finish();
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        //Log.i("js", "2");
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        // TODO Auto-generated catch block
                        //Log.i("js", "3");
                        e.printStackTrace();
                    }


                    finish();
                }
                break;

            default:
                break;
        }

    }

}
