package com.example.anb.buy;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;

public class BuyTabsPagerAdapter extends FragmentPagerAdapter {
	ViewPager viewPager;
	public BuyTabsPagerAdapter(FragmentManager fm,ViewPager viewPager) {
        super(fm);
        this.viewPager=viewPager; 
    }
	
	
	@Override
	public Fragment getItem(int index) {
		switch(index) {
		case 0:
			Log.i("subact1","done");
			//return new BuyAct1(viewPager);
		case 1:
			Log.i("subact2","done");
			//return new BuyAct2(viewPager);
		case 2:
			Log.i("subact3","done");
			//return new BuyAct3(viewPager);
		case 3:
			Log.i("subact4","done");
			//return new BuyAct4(viewPager);
		default:
			return null;
		}
	}
	
	@Override
    public int getCount() {
        // get item count - equal to number of tabs
        return 4;
    }
}
