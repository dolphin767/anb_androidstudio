package com.example.anb.buy;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.example.anb.R;

public class BuyActivity extends FragmentActivity implements ActionBar.TabListener {
	private ViewPager viewPager;
	private BuyTabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	private String[] tabs = {"one", "two", "three", "four"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_layout);
	
		// Initialize.
		viewPager = (ViewPager)this.findViewById(R.id.pager);
		mAdapter = new BuyTabsPagerAdapter(getSupportFragmentManager(), viewPager);
		viewPager.setAdapter(mAdapter);
		viewPager.setOffscreenPageLimit(4);	// The key of all problems in universe!
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
		    @Override
		    public void onPageSelected(int position) {
		        // on changing the page
		        // make respected tab selected
		        // actionBar.setSelectedNavigationItem(position);
				Log.i("onTabSelected", "tab " + position + " selected");
		        actionBar.getTabAt(position).select();
		    }

		    @Override
		    public void onPageScrolled(int arg0, float arg1, int arg2) {
		    }

		    @Override
		    public void onPageScrollStateChanged(int arg0) {
		    }
		});
		
		actionBar = getActionBar();
		//actionBar.setHomeButtonEnabled(false);
		actionBar.setDisplayShowTitleEnabled(false);
		//actionBar.setDisplayShowHomeEnabled(false);
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setIcon(null)
					.setTabListener(this));
		}
	
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState)	{
		super.onSaveInstanceState(outState);
		//outState.putInt("selected Tab ", getActionBar().getSelectedNavigationIndex());
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
        // show respected fragment view
        viewPager.setCurrentItem(tab.getPosition());
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig)	{
		super.onConfigurationChanged(newConfig);
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub
		
	}
}
