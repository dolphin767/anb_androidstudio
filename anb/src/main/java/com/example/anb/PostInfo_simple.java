package com.example.anb;

import android.graphics.Bitmap;

public class PostInfo_simple {
	
	private Bitmap userProfile;
	private String userName;
	private String productName;
	private int productPrice;
	private String userText;
	private Bitmap productPhoto;


	public String getuserName() {
		return userName;
	}
	public void setuserName(String userName) {
		this.userName = userName;
	}
	public int getproductPrice() {
		return productPrice;
	}
	public void setproductPrice(int productPrice) {
		this.productPrice = productPrice;
	}
	public String getproductName() {
		return productName;
	}
	public void setproductName(String productName) {
		this.productName = productName;
	}
	public Bitmap getuserProfile() {
		return userProfile;
	}
	public void setuserProfile(Bitmap userProfile) {
		this.userProfile = userProfile;
	}
	public Bitmap getproductPhoto() {
		return productPhoto;
	}
	public void setproductPhoto(Bitmap productPhoto) {
		this.productPhoto = productPhoto;
	}

	public String getuserText() {
		return userText;
	}
	public void setuserText(String userText) {
		this.userText =userText;
	}
	

}
