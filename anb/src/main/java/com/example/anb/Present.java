package com.example.anb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.anb.present.PresentActivity;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


public class Present extends Fragment {
	private ArrayList<PresentInfo> presentList;
	private PresentAdapter pAdapter;
	private ListView myList;
	final int maxPresentNumber = 5;
	final int MAX_PRESENT = 1000;
	View v;
	private ProgressDialog pDialog;
	String JSONString;
    int mode;
	public Present(String JSONString) {
		this.JSONString = JSONString;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.presentlist, container, false);
		String[] showlist = getResources().getStringArray(R.array.presentArray);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item,showlist);
		adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		presentList = new ArrayList<PresentInfo>();
		Spinner mode_select = (Spinner)v.findViewById(R.id.present_spinner);
		mode_select.setAdapter(adapter);
		mode_select.setOnItemSelectedListener(new OnItemSelectedListener(){
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedView, int position, long id){
				Spinner sp = (Spinner)v.findViewById(R.id.present_spinner);
				String resultText = "";
				if(sp.getSelectedItemPosition()>=0){
					resultText=(String)sp.getAdapter().getItem(sp.getSelectedItemPosition());
				//	Toast.makeText(v.getContext(), "resultText :" + resultText + sp.getSelectedItemPosition(), Toast.LENGTH_LONG).show();//�ش������������
					//pAdapter.setMode(sp.getSelectedItemPosition());
                   // presentList.clear();
                    //pAdapter.notifyDataSetInvalidated();
					mode = sp.getSelectedItemPosition();
                    if(mode == 0) {
                        presentList.clear();
						Toast.makeText(v.getContext(), "0", Toast.LENGTH_SHORT).show();
                        setPresentList("test", "give");
                        setPresentList("test", "take");
                        setPresentList("test", "give");
                        setPresentList("test", "give");
                        setPresentList("test", "take");
						pAdapter.notifyDataSetInvalidated();
                    }
                    else if (mode == 1){

						presentList.clear();
						Toast.makeText(v.getContext(), "1", Toast.LENGTH_SHORT).show();
						setPresentList("test", "take");
                        setPresentList("test", "take");
						pAdapter.notifyDataSetInvalidated();
                    }else if (mode == 2){
                        presentList.clear();
						Toast.makeText(v.getContext(), "2", Toast.LENGTH_SHORT).show();
                        setPresentList("test", "give");
                        setPresentList("test", "give");
                        setPresentList("test", "give");
						pAdapter.notifyDataSetInvalidated();
                    }
                    pAdapter.notifyDataSetChanged();
                }
			}
			public void onNothingSelected(AdapterView<?> parentView){
				
			}
			
		});
		myList = (ListView) v.findViewById(R.id.present_listView);
		pAdapter = new PresentAdapter(getActivity(), R.layout.present_info, presentList);
		myList.setAdapter(pAdapter);
		
		ArrayList<String> postInfo = new ArrayList<String>();
		ArrayList<String> postInfo2 = new ArrayList<String>();

		
		String[] pid = new String[5];
		String[] anb = new String[5];
		int array_len = 0;
		String result = "";
		String result2 = "";
		
				
		Button presentButton = (Button) v.findViewById(R.id.btnPresent);
		presentButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), PresentActivity.class);
				startActivity(intent);
			}
		});

		try {
			JSONArray ja3 = new JSONArray(result2);
			for (int i = array_len - 1; i >= 0; i--) {
				setPresentList(ja3.getJSONObject(i).toString(), anb[i]);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	private void setPresentList(String result, String anb) {
		
		Bitmap give_default = BitmapFactory.decodeResource(getActivity()
				.getResources(), R.drawable.present_give);
		Bitmap take_default = BitmapFactory.decodeResource(getActivity()
				.getResources(), R.drawable.present_take);
		
		//try {
			//JSONObject jsonobject = new JSONObject(result);
			PresentInfo presentItem = new PresentInfo();

			presentItem.setName("재성");
			presentItem.setproductName("사과");
			presentItem.setproductSize("3");
			presentItem.setpresentPhoto(give_default);
			if(anb.compareTo("give")==0){
				presentItem.setpresentPhoto(give_default);
				presentItem.setSort(true);
			}
			else{
				presentItem.setpresentPhoto(take_default);
				presentItem.setSort(false);
			}
			presentList.add(presentItem);

			pAdapter.notifyDataSetChanged();
		//} catch (JSONException e) {
			// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}

	}

}