package com.example.anb.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ViewFlipper;

import com.example.anb.R;

// View.OnTouchListener 인터페이스와
// CompoundButton.OnCheckedChangeListener 인터페이스 구현 함
public class ViewFlipperActivity extends Activity implements View.OnClickListener {
    final int MAX = 3;
    Button btnPrev, btnNext;
    ViewFlipper vf;
    int cnt = 0;
    Animation slide_in_left, slide_out_right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_layout);

        btnPrev = (Button) findViewById(R.id.prev);
        btnNext = (Button) findViewById(R.id.next);
        vf = (ViewFlipper) findViewById(R.id.viewflipper);


        btnPrev.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.view_menu, menu);
        return true;
    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.prev:
                if(cnt > 0) {
                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_right_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt--;
                    vf.showPrevious();

                }
                else
                    finish();
                break;
            case R.id.next:
                if(cnt < MAX-1) {
                    slide_in_left = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_in);
                    slide_out_right = AnimationUtils.loadAnimation(this,
                            R.anim.push_left_out);

                    vf.setInAnimation(slide_in_left);
                    vf.setOutAnimation(slide_out_right);
                    cnt++;
                    vf.showNext();
                }
                else
                finish();
                break;

            default:
                break;
        }

    }

}
