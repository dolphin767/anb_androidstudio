package com.example.anb;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

/**
 * Created by minseock on 2015-04-03.
 */
public class FriendAdapter extends ArrayAdapter<Friend> {

    private ArrayList<Friend> items;
    private DisplayImageOptions options;

    public FriendAdapter(Context context, int resource,ArrayList<Friend> items) {
        super(context, resource,items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        options = new DisplayImageOptions.Builder()
                .cacheInMemory()
                .cacheOnDisc()
                .build();
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_friend, null);
        }
        Friend p = items.get(position);
        if (p != null) {
            TextView tt = (TextView) v.findViewById(R.id.name_friend);
            ImageView iv = (ImageView) v.findViewById(R.id.profile_picture);
            if (tt != null){
                tt.setText(p.getName());
                ImageLoader.getInstance().displayImage(p.getProfilePictureSource(), iv,options);
            }
        }
        return v;

    }
}
