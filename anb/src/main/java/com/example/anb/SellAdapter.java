package com.example.anb;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.etsy.android.grid.util.DynamicHeightTextView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by edward on 2015-04-02.
 */
public class SellAdapter extends ArrayAdapter<PostInfo_simple> {

    private static final String TAG = "SampleAdapter";

    static class ViewHolder {
        DynamicHeightImageView imgOne;
        DynamicHeightTextView txtLineOne;
        Button btnGo;
    }

    private final LayoutInflater mLayoutInflater;
    private final Random mRandom;
    private final ArrayList<Integer> mBackgroundColors;
    Bitmap img_default = BitmapFactory.decodeResource(Resources.getSystem(), R.drawable.present_give);
     private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public SellAdapter(final Context context, final int textViewResourceId) {
        super(context, textViewResourceId);
        mLayoutInflater = LayoutInflater.from(context);
        mRandom = new Random();
        mBackgroundColors = new ArrayList<Integer>();
        mBackgroundColors.add(1);
        mBackgroundColors.add(2);
        mBackgroundColors.add(3);
        mBackgroundColors.add(4);
        mBackgroundColors.add(5);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder vh;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item_buy, parent, false);

            vh = new ViewHolder();
            vh.imgOne = (DynamicHeightImageView) convertView.findViewById(R.id.img_one);
            vh.txtLineOne = (DynamicHeightTextView) convertView.findViewById(R.id.txt_line1);

            convertView.setTag(vh);
        }
        else {
            vh = (ViewHolder) convertView.getTag();
        }

        double positionHeight = getPositionRatio(position);
        int backgroundIndex = position >= mBackgroundColors.size() ?
                position % mBackgroundColors.size() : position;

        convertView.setBackgroundColor(Color.parseColor("#777777"));

        Log.d(TAG, "getView position:" + position + " h:" + positionHeight);

        vh.imgOne.setHeightRatio(positionHeight);
        // vh.imgOne.setImageBitmap(getItem(position).getproductPhoto());
        vh.txtLineOne.setHeightRatio(positionHeight);
        if(position == 0) vh.txtLineOne.setText(getItem(position).getuserText());
        else vh.txtLineOne.setText(getItem(position).getproductName() + "가 " + getItem(position).getproductPrice() +"원 "+getItem(position).getuserText());
        /*
        vh.btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Toast.makeText(getContext(), "Button Clicked Position " +
                        position, Toast.LENGTH_SHORT).show();
            }
        });
        */

        return convertView;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
            Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }
}