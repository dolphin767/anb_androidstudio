package com.example.anb;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.util.ArrayList;

public class SampleData {

    public static final int SAMPLE_DATA_ITEM_COUNT = 30;

    public static ArrayList<PostInfo_simple> generateSampleData() {
        final ArrayList<PostInfo_simple> data = new ArrayList<PostInfo_simple>(SAMPLE_DATA_ITEM_COUNT);
        PostInfo_simple info = new PostInfo_simple();
        Bitmap give_default = BitmapFactory.decodeResource(Resources.getSystem(),R.drawable.present_give);
        Bitmap take_default = BitmapFactory.decodeResource(Resources.getSystem(),R.drawable.present_give);


        for (int i = 0; i < SAMPLE_DATA_ITEM_COUNT; i++) {
            info.setproductName("오렌지");
            info.setproductPhoto(give_default);
            info.setproductPrice(5000);
            info.setuserName("재성");
            info.setuserProfile(take_default);
            info.setuserText("오렌지가 맛나요 대박");
            data.add(info);
        }

        return data;
    }

}