package com.example.anb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;

import java.util.ArrayList;

public class Buy extends Fragment {
	View v;
	private ProgressDialog pDialog;
	String JSONString;
    private StaggeredGridView mGridView;
    private boolean mHasRequestedMore;
    private BuyAdapter mAdapter;
    private ArrayList<PostInfo_simple> mData;

    public Buy(String JSONString) {
		this.JSONString = JSONString;
	}
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_buy, container, false);
		return v;

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGridView = (StaggeredGridView) getView().findViewById(R.id.grid_view);
        PostInfo_simple code_data = new PostInfo_simple();
        code_data.setuserText("코드입력");
        //code_data.setuserProfile("");
        code_data.setuserName("");
        code_data.setproductPrice(0);
        code_data.setproductName("");
        /*
        if (savedInstanceState == null) {
            final LayoutInflater layoutInflater = getActivity().getLayoutInflater();

            View header = layoutInflater.inflate(R.layout.list_item_header_footer, null);
            View footer = layoutInflater.inflate(R.layout.list_item_header_footer, null);
            TextView txtHeaderTitle = (TextView) header.findViewById(R.id.txt_title);
            TextView txtFooterTitle = (TextView) footer.findViewById(R.id.txt_title);
            txtHeaderTitle.setText("THE HEADER!");
            txtFooterTitle.setText("THE FOOTER!");

            mGridView.addHeaderView(header);
            mGridView.addFooterView(footer);
        }
        */

        if (mAdapter == null) {
            mAdapter = new BuyAdapter(getActivity(), R.id.txt_line1);
        }

        if (mData == null) {
            mData = SampleData.generateSampleData();
        }
        mAdapter.add(code_data);
        for (PostInfo_simple data : mData) {
            mAdapter.add(data);
        }

        mGridView.setAdapter(mAdapter);
        //mGridView.setOnScrollListener(this);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                Toast.makeText(getActivity(), position+"번", Toast.LENGTH_SHORT).show();
                if(id == 0) {
                    Intent intent = new Intent(getActivity(), CodeActivity.class);
                   intent.putExtra("product_info", id);
                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent(getActivity(), Product_Info_Activity_Buy.class);
                    intent.putExtra("product_info", id);
                    startActivity(intent);

                }


            }
        });

	}

}