package com.example.anb.navidrawer;

/**
 * Created by user on 2015-11-04.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.anb.Post;
import com.example.anb.R;

import java.util.ArrayList;

public class PasswordEditActivity extends Activity {
    String num = "0";

    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.password_change);
        Button button_complete = (Button) findViewById(R.id.btnComplete);


        button_complete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PasswordEditActivity.this, MyPage.class);
               EditText password_change = (EditText) findViewById(R.id.password_change);
                EditText password_check = (EditText) findViewById(R.id.password_check);
                try {

                    String JSONString = null;
                    String URL = "http://54.178.153.85:3000/users/modifypassword"; // server
                    // URL
                    ArrayList<String> postInfo = new ArrayList<String>();
                    postInfo.add(URL);
                    postInfo.add(num);

                    if (password_change.getText().toString() != "" && password_check.getText().toString() != "") { // 아무것도 입력 받지 않은 상황에서 "" 가 아닌 다른 값 입력받음. 로그찍을것
                        if(password_change.getText().toString().equals(password_check.getText().toString())){
                            //패스워드 체크와 새로운 패스워드 일치
                            postInfo.add(password_change.getText().toString());
                            Post post = new Post();
                            //JSONString = post.execute(postInfo).get();

                            //JSONObject jo = new JSONObject(JSONString);
//					    if (jo.length() == 0) {
//						Toast.makeText(LoginActivity.this, "Login Failed!",
//								Toast.LENGTH_SHORT).show();
//						startActivity(intent);
//						finish();
//					    } else {

                            //intent.putExtra("user_info", JSONString);
                            Toast.makeText(v.getContext(), "Password Changed", Toast.LENGTH_SHORT).show();
                            startActivity(intent);
                            finish();

                        }else
                            Toast.makeText(v.getContext(), "Different", Toast.LENGTH_SHORT).show();



                    } else   Toast.makeText(v.getContext(), "Missing input", Toast.LENGTH_SHORT).show();

//					}

                } catch (Exception e) {
                    e.printStackTrace();
                }
               // startActivity(intent);

            }
        });


    }
}

