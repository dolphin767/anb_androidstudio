package com.example.anb.navidrawer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anb.R;

/**
 * Created by edward on 2015-07-10.
 */
public class MyPage extends Activity {
    String JSONString;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.mypage_layout);
        Button button = (Button) findViewById(R.id.btnEdit);
        Button passwordButton = (Button) findViewById(R.id.btnPasswordEdit);
        TextView name = (TextView)findViewById(R.id.user_name);
        TextView id = (TextView)findViewById(R.id.user_id);
        TextView addr = (TextView)findViewById(R.id.user_addr);
        TextView phone = (TextView)findViewById(R.id.user_phone);
        TextView gender = (TextView)findViewById(R.id.user_gender);
        ImageView profile = (ImageView)findViewById(R.id.person_profil);
        name.setText("Choi Min Seok");
        id.setText("minchoi@naver.com");
        addr.setText("Seoul, Wangshiplee station");
        phone.setText("01041232322");
        gender.setText("Male");
        Bitmap profile_default = BitmapFactory.decodeResource(this
                .getResources(), R.drawable.profile_default);
        profile.setImageBitmap(profile_default);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Edit", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MyPage.this, ProfileEditActivity.class);
                startActivity(intent);
                // finish();

            }
        });
        passwordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "EditPassword", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MyPage.this, PasswordEditActivity.class);
                startActivity(intent);
                // finish();

            }
        });
    }
}
