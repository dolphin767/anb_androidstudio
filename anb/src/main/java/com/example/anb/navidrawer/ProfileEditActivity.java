package com.example.anb.navidrawer;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.anb.Post;
import com.example.anb.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by edward on 2015-07-10.
 */
public class ProfileEditActivity  extends Activity {
    String num = "0";
    String email = "";
    String fid = "";
    String addr = "";
    String phone = "";
    String img = "";

    private static final int SELECT_PICTURE = 1;
    public void onCreate(final Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.mypage_edit_layout);
        Button button = (Button) findViewById(R.id.btnEdit);
        TextView name_edit = (TextView)findViewById(R.id.user_name);

        TextView gender = (TextView)findViewById(R.id.user_gender);
        ImageView profile = (ImageView)findViewById(R.id.person_profil);

        Bitmap profile_default = BitmapFactory.decodeResource(this
                .getResources(), R.drawable.ic_camera_01);
        profile.setImageBitmap(profile_default);
        gender.setText("male");
        name_edit.setText("Choi Min Seok");

        profile.setOnClickListener((new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
                        intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_PICTURE);

                    }
                })
        );
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Edit", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(ProfileEditActivity.this, MyPage.class);
                EditText id_edit = (EditText)findViewById(R.id.user_id);
                EditText addr_edit = (EditText)findViewById(R.id.user_addr);
                EditText phone_edit = (EditText)findViewById(R.id.user_phone);
                try {

                    String JSONString = null;
                    String URL = "http://54.178.153.85:3000/users/modify"; // server
                    // URL
                    ArrayList<String> postInfo = new ArrayList<String>();
                    postInfo.add(URL);
                    postInfo.add(num);
                    if(id_edit.getText().toString()!="")postInfo.add(id_edit.getText().toString());
                    else postInfo.add(email);
                    postInfo.add(fid);
                    if(addr_edit.getText().toString()!="")postInfo.add(addr_edit.getText().toString());
                    else postInfo.add(addr);
                    if(phone_edit.getText().toString()!="")postInfo.add(phone_edit.getText().toString());
                    else postInfo.add(phone);
                    if(img!="")postInfo.add(img);
                    else postInfo.add(img);
                    Post post = new Post();
                    //JSONString = post.execute(postInfo).get();

                    //JSONObject jo = new JSONObject(JSONString);
//					if (jo.length() == 0) {
//						Toast.makeText(LoginActivity.this, "Login Failed!",
//								Toast.LENGTH_SHORT).show();
//						startActivity(intent);
//						finish();
//					} else {

                    //intent.putExtra("user_info", JSONString);

                    startActivity(intent);
                    finish();
//					}

                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(intent);

            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        Toast.makeText(getBaseContext(), "resultCode : "+resultCode,Toast.LENGTH_SHORT).show();

        if(requestCode == SELECT_PICTURE)
        {
            if(resultCode==Activity.RESULT_OK)
            {
                try {

                    Bitmap image_bitmap 	= MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                    ImageView profile = (ImageView)findViewById(R.id.person_profil);
                     profile.setImageBitmap(image_bitmap);
                    img = BitMapToString(image_bitmap);

                    //Toast.makeText(getBaseContext(), "name_Str : "+name_Str , Toast.LENGTH_SHORT).show();


                } catch (FileNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }


    public String getImageNameToUri(Uri data)
    {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(data, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String imgPath = cursor.getString(column_index);
        String imgName = imgPath.substring(imgPath.lastIndexOf("/") + 1);

        return imgName;
    }
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte [] b=baos.toByteArray();
        String temp=Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}


