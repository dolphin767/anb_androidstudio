package com.example.anb;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.example.anb.sell.SellActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewsFeed extends Fragment {
	private ArrayList<PostInfo> postList;
	private PostAdapter pAdapter;
	private ListView myWall;
	final int maxPostNumber = 5;
	final int MAX_POST = 1000;
	View v;
	private ProgressDialog pDialog;
	String JSONString;

	public NewsFeed(String JSONString) {
		this.JSONString = JSONString;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.postlist, container, false);
		postList = new ArrayList<PostInfo>();
		myWall = (ListView) v.findViewById(R.id.timeLine_listView);
		pAdapter = new PostAdapter(getActivity(), R.layout.post, postList);
		myWall.setAdapter(pAdapter);
		String URL = "http://54.178.153.85:3000/main";
		String URL2 = "http://54.178.153.85:3000/main/products";
		Log.i("js", "received on Newsfeed " + JSONString);
		ArrayList<String> postInfo = new ArrayList<String>();
		ArrayList<String> postInfo2 = new ArrayList<String>();

		JSONObject jo;
		JSONArray ja;
		String[] pid = new String[MAX_POST];
		String[] anb = new String[MAX_POST];
		int array_len = 0;
		String result = "";
		String result2 = "";
		try {
			jo = new JSONObject(JSONString);
			String b_num = jo.getString("bid");
			Log.i("js", "send " + b_num);
			postInfo.add(URL);
			postInfo.add(b_num);
			Post post = new Post();
			result = post.execute(postInfo).get();
			ja = new JSONArray(result);
			Log.i("js", "result is " + result);
			if (ja.length() > MAX_POST)
				array_len = MAX_POST;
			else
				array_len = ja.length();
			for (int i = 0; i < array_len; i++) {
				pid[i] = ja.getJSONObject(i).getString("product");
				anb[i] = "";
				if (ja.getJSONObject(i).length() == 2)
					anb[i] = ja.getJSONObject(i).getString("anb");
			}
			postInfo2.add(URL2);
			JSONArray ja2 = new JSONArray();

			Post post2 = new Post();
			for (int i = 0; i < array_len; i++) {
				Log.i("js", "array " + i + " is " + pid[i]);
				JSONObject jo2 = new JSONObject();
				jo2.put("product", pid[i]);
				ja2.put(jo2);
			}

			Log.i("js", "ja2 is " + ja2.toString());
			postInfo2.add(ja2.toString());
			result2 = post2.execute(postInfo2).get();
			Log.i("js", "result 2 is" + result2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("js", e.toString());
		}
		Button sellButton = (Button) v.findViewById(R.id.btnSell);
		sellButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), SellActivity.class);
				Log.i("js", "News json" + JSONString + "onclick");
				intent.putExtra("user_info", JSONString);
				startActivity(intent);
			}
		});
		try {
			JSONArray ja3 = new JSONArray(result2);
			for (int i = array_len - 1; i >= 0; i--) {
				setPostsToWall(ja3.getJSONObject(i).toString(), anb[i]);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

	}

	private void setPostsToWall(String result, String anb) {
		//postList.clear();
		Bitmap profile_default = BitmapFactory.decodeResource(getActivity()
				.getResources(), R.drawable.profile_default);
		Bitmap product_default = BitmapFactory.decodeResource(getActivity()
				.getResources(), R.drawable.product_default);
		try {
			JSONObject jsonobject = new JSONObject(result);
			PostInfo postItem = new PostInfo();

			postItem.setuserName(jsonobject.getString("name"));
			
			if (anb != "")
				postItem.setuserFriend(anb);
			else
				postItem.setuserFriend("");
			postItem.setproductName(jsonobject.getString("product_name"));
			postItem.setproductPrice(jsonobject.getInt("price"));
			postItem.setproductSize(jsonobject.getString("size"));
			postItem.setuserText(jsonobject.getString("description"));
			postItem.setuserTime(jsonobject.getString("time"));
			postItem.setuserWhere(jsonobject.getString("place"));
			postItem.setproductPhoto(product_default);
			postItem.setuserProfile(profile_default);
			postList.add(postItem);

			pAdapter.notifyDataSetChanged();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}