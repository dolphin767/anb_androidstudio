package com.example.anb;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by edward on 2015-04-02.
 */
public class Product_Info_Activity_Sell extends Activity {
        private static final String Tag = "ProductInformation";
        private PostInfo user;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.sell_product_info);
        TextView userName = (TextView) findViewById(R.id.user_id);
        TextView userTime = (TextView) findViewById(R.id.user_time);
        TextView userFriend = (TextView)findViewById(R.id.user_friend);
        TextView userWhere = (TextView) findViewById(R.id.user_where);
        TextView userText = (TextView) findViewById(R.id.user_text);
        TextView productInfo = (TextView) findViewById(R.id.product_info);
        ImageView userProfile = (ImageView) findViewById(R.id.person_profil);
        ImageView productPhoto = (ImageView) findViewById(R.id.product_photo);

        Bitmap profile_default = BitmapFactory.decodeResource(this
                .getResources(), R.drawable.profile_default);
        Bitmap product_default = BitmapFactory.decodeResource(this
                .getResources(), R.drawable.product_default);


        userName.setText("황재성");
        userTime.setText("4월1일 오후 3시 30분");
        userFriend.setText("김유정");
        userWhere.setText("강원도 속초");
        userText.setText("별그대 전지현씨가 먹어서 유행이 된 맛있는 사과!\n" +
                "맛도 좋고 향도 좋고 당도 조차 화제가 되고 있는 사과입니다.\n" +
                "지난달에 수확하였고 사진에서 볼 수 있듯이 상태 매우 좋습니다.\n" +
                "온 가족과 함께 즐겨보세요.\n");
        productInfo.setText("이름: 사과\n" +
                "수량: 3\n" +
                "가격: 3,000\n");
        userProfile.setImageBitmap(profile_default);
        productPhoto.setImageBitmap(product_default);
            /*
            userName.setText(user.getuserName());
            userTime.setText(user.getuserTime());
            userFriend.setText(user.getuserFriend());
            userWhere.setText(user.getuserWhere());
            userText.setText(user.getuserText());
            productInfo.setText("이름:" + user.getproductName() + " 수량"
                    + user.getproductSize() + " 가격:" + user.getproductPrice());
            userProfile.setImageBitmap(user.getuserProfile());
            productPhoto.setImageBitmap(user.getproductPhoto());
            */
        Button Delete = (Button) findViewById(R.id.btnDelete);
        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //삭제하기 프로세스
                Product_Info_Activity_Sell.this.finish();
                Toast.makeText(Product_Info_Activity_Sell.this, "Post edit", Toast.LENGTH_SHORT).show();

            }
        });
        Button Edit = (Button) findViewById(R.id.btnEdit);
        Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //고치기
                //레이아웃 새로 생성 요
                Product_Info_Activity_Sell.this.finish();
                Toast.makeText(Product_Info_Activity_Sell.this, "Post deleted", Toast.LENGTH_SHORT).show();
            }
        });

}
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
    }

}
