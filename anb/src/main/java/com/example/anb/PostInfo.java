package com.example.anb;

import android.graphics.Bitmap;

public class PostInfo {
	
	private Bitmap userProfile;
	private String userName;
	private String userFriend;
	private String userTime;
	private String userWhere;
	private String productName;
	private String productSize;
	private int productPrice;
	private String userText;
	private Bitmap productPhoto;


	public String getuserName() {
		return userName;
	}
	public void setuserName(String userName) {
		this.userName = userName;
	}
	public int getproductPrice() {
		return productPrice;
	}
	public void setproductPrice(int productPrice) {
		this.productPrice = productPrice;
	}
	public String getproductSize() {
		return productSize;
	}
	public void setproductSize(String productSize) {
		this.productSize = productSize;
	}
	public String getproductName() {
		return productName;
	}
	public void setproductName(String productName) {
		this.productName = productName;
	}
	public String getuserFriend() {
		return userFriend;
	}
	public void setuserFriend(String userFriend) {
		this.userFriend = userFriend;
	}
	public Bitmap getuserProfile() {
		return userProfile;
	}
	public void setuserProfile(Bitmap userProfile) {
		this.userProfile = userProfile;
	}
	public Bitmap getproductPhoto() {
		return productPhoto;
	}
	public void setproductPhoto(Bitmap productPhoto) {
		this.productPhoto = productPhoto;
	}
	public String getuserWhere() {
		return userWhere;
	}
	public void setuserWhere(String userWhere) {
		this.userWhere = userWhere;
	}
	
	public String getuserTime() {
		return userTime;
	}
	public void setuserTime(String userTime) {
		this.userTime = userTime;
	}
	
	public String getuserText() {
		return userText;
	}
	public void setuserText(String userText) {
		this.userText =userText;
	}
	

}
