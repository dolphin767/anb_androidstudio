package com.example.anb;


public class TradeInfo {
	private int status;
	private String name;
	private String eventTime;
	private String productName;
	private String productSize;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getproductSize() {
		return productSize;
	}
	public void setproductSize(String productSize) {
		this.productSize = productSize;
	}
	public String getproductName() {
		return productName;
	}
	public void setproductName(String productName) {
		this.productName = productName;
	}
	public String geteventTime() {
		return eventTime;
	}
	public void seteventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	
}
