package com.example.anb;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class PresentAdapter extends ArrayAdapter<PresentInfo> {

	private Context mContext;
	private int mResource;
	private ArrayList<PresentInfo> mList;
	private LayoutInflater mInflater;
    private int mode;
	public PresentAdapter(Context context, int layoutResource,
			ArrayList<PresentInfo> objects) {
		super(context, layoutResource, objects);
		this.mContext = context;
		this.mResource = layoutResource;
		this.mList = objects;
		this.mInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
    public void setMode(int mode){
        this.mode = mode;
        
    }
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		PresentInfo user = mList.get(position);

		if (convertView == null) {
			convertView = mInflater.inflate(mResource, null);
		}

		if (user != null && mode == 0) {
            Log.e("mode", "0");
			ImageView presentPhoto = (ImageView) convertView
					.findViewById(R.id.present_photo);
			TextView presentText = (TextView) convertView
					.findViewById(R.id.present_info);

			if (user.getSort()) {
				presentText.setText(user.getName() + " sent product " + user.getproductName()
						+ " " + user.getproductSize() + "kg");
				presentPhoto.setImageBitmap(user.getpresentPhoto());
			} else {
				presentText.setText(user.getName() + " received product " + user.getproductName()
						+ " " + user.getproductSize() + "kg");
				presentPhoto.setImageBitmap(user.getpresentPhoto());

			}
		}else if (user != null && mode == 1) {
            Log.e("mode", "1");
            ImageView presentPhoto = (ImageView) convertView
                    .findViewById(R.id.present_photo);
            TextView presentText = (TextView) convertView
                    .findViewById(R.id.present_info);

            if (user.getSort()) {
                presentText.setText(user.getName() + " sent product " + user.getproductName()
                        + " " + user.getproductSize() + "kg");
                presentPhoto.setImageBitmap(user.getpresentPhoto());
            }
        }else if (user != null && mode == 2) {
            Log.e("mode", "2");
            ImageView presentPhoto = (ImageView) convertView
                    .findViewById(R.id.present_photo);
            TextView presentText = (TextView) convertView
                    .findViewById(R.id.present_info);

           if (!user.getSort()){
               presentText.setText(user.getName() + " received product " + user.getproductName()
                       + " " + user.getproductSize() + "kg");
               presentPhoto.setImageBitmap(user.getpresentPhoto());

            }
        }else return convertView;

		return convertView;
	}
}